import { createVuetify, ThemeDefinition } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const myAllBlackTheme: ThemeDefinition = {
    dark: false,
    colors: {
        background: '#ffffff',
        surface: '#eeeeee',
        primary: '#e20774',
        'cta': '#ff3300',
        secondary: '#456782',
        error: '#ff3300',
        info: '#ffe200',
        success: '#78ff24',
        warning: '#ff6f00',
    }
}
export default defineNuxtPlugin(nuxtApp => {
    const vuetify = createVuetify({
        theme: {
            defaultTheme: 'myAllBlackTheme',
            themes: {
                myAllBlackTheme,
            }
        },
        components,
        directives,
    })

    nuxtApp.vueApp.use(vuetify)
})
