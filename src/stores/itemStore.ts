import { Item } from "~/models/item.model";
import { defineStore } from "#imports";

export type RootState = {
  items: Item[];
};

export const useItemStore = defineStore({
  id: "itemStore",
  state: () =>
    ({
      items: [],
    } as RootState),

  actions: {
    createItem(item: Item) {
      if (!item) return;
      this.items.push(item);
    },
    getItems() {
      return this.items;
    },
  },
});
