# The Nuxt3 Basic Starter

## Stack

- vuetify (with custom theme preset)
- vueuse
- pinia (with autoimport)

## DEV Stack

- typescript
- eslint
- prettier
